require 'rails_helper'

RSpec.feature 'Categories' do
  describe "#show" do
    let!(:taxonomy) { create(:taxonomy) }
    let!(:taxon) { create(:taxon, taxonomy: taxonomy) }
    let!(:product) { create(:product, taxons: [taxon]) }
    let!(:product_else) { create(:product) }

    before do
      visit potepan_category_path(taxon.id)
    end

    it "表示されているのか確認" do
      expect(page).to have_content(taxonomy.name)
      expect(page).to have_content(product.display_price)
      expect(page).to have_content(product.name)
      expect(page).to have_no_content(product_else.name)
    end

    it "詳細ページへのリンクを持っていること" do
      expect(page).to have_link href: potepan_product_path(product.id)
      click_link href: potepan_product_path(product.id)
      expect(current_path).to eq potepan_product_path(product.id)
    end
  end
end
