require 'rails_helper'

RSpec.feature "Products" do
  describe "#show" do
    let!(:taxon) { create(:taxon) }
    let!(:product) { create(:product, taxons: [taxon]) }

    before do
      visit potepan_product_path(product.id)
    end

    it "商品が表示されている" do
      within(".singleProduct") do
        expect(page).to have_content product.name
        expect(page).to have_content product.display_price
        expect(page).to have_content product.description
      end
    end
  end

  describe "関連商品のテスト" do
    let!(:taxon) { create(:taxon) }
    let!(:product) { create(:product, taxons: [taxon]) }
    let!(:related_products) { create(:product, taxons: [taxon]) }

    before do
      visit potepan_product_path(product.id)
    end

    it "関連商品が表示されているか" do
      within(".productsContent") do
        expect(page).to have_content related_products.name
        expect(page).to have_content related_products.display_price
      end
    end

    it "関連商品がリンクを持っているか" do
      within(".productsContent") do
        expect(page).to have_link href: potepan_product_path(related_products.id)
        click_link href: potepan_product_path(related_products.id)
        expect(current_path).to eq potepan_product_path(related_products.id)
      end
    end
  end
end
