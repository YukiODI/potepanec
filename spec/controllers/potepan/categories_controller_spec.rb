require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  describe "GET #show" do
    let!(:taxonomy) { create(:taxonomy) }
    let!(:taxon)    { create :taxon, taxonomy: taxonomy }
    let!(:product)  { create :product, taxons: [taxon] }

    before do
      get :show, params: { id: taxon.id }
    end

    it "showへのリクエストが成功すること" do
      expect(response.status).to eq(200)
    end

    it "showテンプレートで表示されていること" do
      expect(response).to render_template :show
    end

    it "Taxonの変数の確認" do
      expect(assigns(:taxon)).to eq taxon
    end

    it "categoriesの変数の確認" do
      expect(assigns(:categories)).to contain_exactly(taxonomy)
    end

    it "productの変数の確認" do
      expect(assigns(:products)).to contain_exactly(product)
    end
  end
end
