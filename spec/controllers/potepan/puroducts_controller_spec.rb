require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe "GET #show" do
    let!(:taxon) { create(:taxon) }
    let!(:product) { create(:product, name: "MUG", taxons: [taxon]) }
    let!(:product_else) { create(:product, name: "TOTE", taxons: [taxon]) }
    let!(:related_products) { create_list(:product, 5, taxons: [taxon]) }

    before do
      get :show, params: { id: product.id }
    end

    it "showへのリクエストが成功すること" do
      expect(response.status).to eq(200)
    end

    it "showテンプレートで表示されていること" do
      expect(response).to render_template :show
    end

    it "変数の確認" do
      expect(assigns(:product)).to eq product
    end

    it "関連商品が4つ表示されること" do
      expect(assigns(:related_products).count).to eq 4
    end

    it "関連していない商品が含まれていないこと" do
      expect(response.body).not_to include "TOTE"
    end
  end
end
