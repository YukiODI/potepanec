module Spree::ProductDecorator # rubocop:disable Airbnb/ClassOrModuleDeclaredInWrongFile
  def related_products
    Spree::Product.in_taxons(taxons).where.not(id: id).distinct
  end
  Spree::Product.prepend self
end
